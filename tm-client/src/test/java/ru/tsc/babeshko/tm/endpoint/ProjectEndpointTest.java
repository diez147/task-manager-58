package ru.tsc.babeshko.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.tsc.babeshko.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.babeshko.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.babeshko.tm.api.service.IPropertyService;
import ru.tsc.babeshko.tm.dto.request.*;
import ru.tsc.babeshko.tm.dto.response.*;
import ru.tsc.babeshko.tm.enumerated.Status;
import ru.tsc.babeshko.tm.marker.ISoapCategory;
import ru.tsc.babeshko.tm.dto.model.ProjectDTO;
import ru.tsc.babeshko.tm.service.PropertyService;
import ru.tsc.babeshko.tm.util.DateUtil;

import java.util.Date;

@Category(ISoapCategory.class)
public final class ProjectEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final String host = propertyService.getServerHost();

    @NotNull
    private final String port = Integer.toString(propertyService.getServerPort());

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @Nullable
    private String token;

    @Nullable
    private ProjectDTO initProject;

    @Before
    public void init() {
        @NotNull final UserLoginResponse loginResponse = authEndpoint.login(new UserLoginRequest("user", "user"));
        token = loginResponse.getToken();
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectCreateResponse createResponse = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "test", "test", null, null)
        );
        initProject = createResponse.getProject();
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(null, initProject.getId(), Status.IN_PROGRESS)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.changeProjectStatusById(
                        new ProjectChangeStatusByIdRequest(token, initProject.getId(), null)));
        ProjectChangeStatusByIdResponse response = projectEndpoint.changeProjectStatusById(
                new ProjectChangeStatusByIdRequest(token, initProject.getId(), Status.IN_PROGRESS));
        Assert.assertNotNull(response);
        @Nullable ProjectDTO project = response.getProject();
        Assert.assertNotEquals(initProject.getStatus(), project.getStatus());
    }


    @Test
    public void createProject() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(null, "", "", null, null)));
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.createProject(
                        new ProjectCreateRequest(token, "", "", null, null)));
        @NotNull final String projectName = "name";
        @NotNull final Date dateBegin = DateUtil.toDate("14.05.2020");
        @NotNull final Date dateEnd = DateUtil.toDate("14.05.2021");
        @NotNull final ProjectCreateResponse response = projectEndpoint.createProject(
                new ProjectCreateRequest(token, "name", "description", dateBegin, dateEnd));
        Assert.assertNotNull(response);
        @Nullable ProjectDTO project = response.getProject();
        Assert.assertEquals(projectName, project.getName());
    }

    @Test
    public void removeProjectByIdTest() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(null, initProject.getId())));
        projectEndpoint.removeProjectById(new ProjectRemoveByIdRequest(token, initProject.getId()));
        Assert.assertNull(projectEndpoint.listProject(new ProjectListRequest(token, null)).getProjects());
    }

    @Test
    public void showProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.showProjectById(new ProjectShowByIdRequest(null, initProject.getId())));
        @NotNull final ProjectShowByIdResponse response = projectEndpoint.showProjectById(
                new ProjectShowByIdRequest(token, initProject.getId()));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getProject());
    }

    @Test
    public void updateProjectById() {
        Assert.assertThrows(Exception.class,
                () -> projectEndpoint.updateProjectById(
                        new ProjectUpdateByIdRequest(null, initProject.getId(), "", "")));
        @NotNull final ProjectUpdateByIdResponse response = projectEndpoint.updateProjectById(
                new ProjectUpdateByIdRequest(token, initProject.getId(), "new_name", "new_description"));
        Assert.assertNotNull(response);
        @Nullable ProjectDTO project = response.getProject();
        Assert.assertNotNull(project);
        Assert.assertNotEquals(initProject.getName(), project.getName());
    }

}