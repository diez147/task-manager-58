package ru.tsc.babeshko.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataYamlLoadFasterXmlRequest extends AbstractUserRequest {

    public DataYamlLoadFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}