package ru.tsc.babeshko.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractUserRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

}