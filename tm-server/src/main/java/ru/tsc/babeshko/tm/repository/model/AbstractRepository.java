package ru.tsc.babeshko.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.babeshko.tm.api.repository.model.IRepository;
import ru.tsc.babeshko.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;

@Getter
@Repository
@Scope("prototype")
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        clear();
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}